#pragma once

typedef DWORD SYSCALL_IF_VERSION;

#define SYSCALL_IMPLEMENTED_IF_VERSION      0x1

#define UM_INVALID_HANDLE_VALUE             0

#define UM_FILE_HANDLE_STDOUT               (UM_HANDLE)0x1

typedef QWORD UM_HANDLE;

typedef enum _UM_HANDLE_TYPE
{
	HandleTypeThread = 1,
	HandleTypeProcess,
	HandleTypeFile
} UM_HANDLE_TYPE;

typedef struct _UM_HANDLE_LIST_ENTRY
{
	LIST_ENTRY		ListEntry;
	PVOID			Ptr;
	UM_HANDLE		Handle;
	UM_HANDLE_TYPE	Type;
} UM_HANDLE_LIST_ENTRY, *PUM_HANDLE_LIST_ENTRY;

#include "mem_structures.h"
#include "thread_defs.h"
#include "process_defs.h"

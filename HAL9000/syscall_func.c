#include "HAL9000.h"
#include "syscall_defs.h"
#include "syscall_func.h"

#include "process_internal.h"
#include "thread_internal.h"
#include "thread.h"
#include "mmu.h"
#include "vmm.h"

STATUS
SyscallValidateInterface(
	IN  SYSCALL_IF_VERSION          InterfaceVersion
)
{
	STATUS status;
	if (InterfaceVersion == SYSCALL_IMPLEMENTED_IF_VERSION)
	{
		status = STATUS_SUCCESS;
	}
	else
	{
		status = STATUS_INCOMPATIBLE_INTERFACE;
	}
	return status;
}

// THREADS

STATUS
SyscallThreadExit(
	IN      STATUS                  ExitStatus
)
{
	PTHREAD pThread = GetCurrentThread();
	PPROCESS Process = pThread->Process;

	if (NULL != Process)
	{
		PUM_HANDLE_LIST_ENTRY pUmHandleListEntry = NULL;
		LIST_ITERATOR ProcessThreadIterator;
		ListIteratorInit(&Process->ProcessThreadDataList, &ProcessThreadIterator);
		PLIST_ENTRY pEntry;
		while ((pEntry = ListIteratorNext(&ProcessThreadIterator)) != NULL)
		{
			PUM_HANDLE_LIST_ENTRY pCurrentData = (PUM_HANDLE_LIST_ENTRY)CONTAINING_RECORD(pEntry, UM_HANDLE_LIST_ENTRY, ListEntry);
			PTHREAD pIteratedThread = (PTHREAD)pCurrentData->Ptr;
			if (pIteratedThread->Id == pThread->Id)
			{
				pUmHandleListEntry = pCurrentData;
				break;
			}
		}

		if (NULL != pUmHandleListEntry)
		{
			ASSERT(pUmHandleListEntry->Type == HandleTypeThread);
			INTR_STATE oldState;
			LockAcquire(&Process->ProcessThreadDataLock, &oldState);

			RemoveEntryList(pEntry);

			LockRelease(&Process->ProcessThreadDataLock, oldState);

			VmmFreeRegionEx(pUmHandleListEntry, sizeof(UM_HANDLE_LIST_ENTRY), VMM_FREE_TYPE_DECOMMIT, FALSE, Process->VaSpace, Process->PagingData);
		}
	}

	ThreadExit(ExitStatus);
	return STATUS_SUCCESS;
}

STATUS
SyscallThreadCreate(
	IN      PFUNC_ThreadStart       StartFunction,
	IN_OPT  PVOID                   Context,
	OUT     UM_HANDLE*              ThreadHandle
)
{
	STATUS status = STATUS_SUCCESS;
	PPROCESS Process = GetCurrentProcess();
	PTHREAD pThread;

#pragma warning(suppress: 4152)
	status = MmuIsBufferValid(StartFunction, sizeof(PFUNC_ThreadStart), PAGE_RIGHTS_ALL, Process);
	if (!SUCCEEDED(status))
	{
		return status;
	}

	pThread = VmmAllocRegion(NULL, sizeof(PTHREAD), VMM_ALLOC_TYPE_ZERO, PAGE_RIGHTS_READWRITE);

	status = ThreadCreateEx("UserThread", ThreadPriorityDefault, StartFunction, Context, &pThread, Process);

	if (SUCCEEDED(status))
	{
		INTR_STATE oldState;
		LockAcquire(&Process->ProcessThreadDataLock, &oldState);

		PUM_HANDLE_LIST_ENTRY pUmHandleListEntry = VmmAllocRegion(NULL, sizeof(UM_HANDLE_LIST_ENTRY), VMM_ALLOC_TYPE_ZERO, PAGE_RIGHTS_READWRITE);
		pUmHandleListEntry->Handle = Process->CurrentHandle++;
		pUmHandleListEntry->Ptr = pThread;
		pUmHandleListEntry->Type = HandleTypeThread;

		InsertTailList(&Process->ProcessThreadDataList, &pUmHandleListEntry->ListEntry);

		LockRelease(&Process->ProcessThreadDataLock, oldState);

		*ThreadHandle = pUmHandleListEntry->Handle;
	}

	return status;
}

STATUS
SyscallThreadGetTid(
	IN_OPT  UM_HANDLE               ThreadHandle,
	OUT     TID*                    ThreadId
)
{
	STATUS status = STATUS_SUCCESS;
	PPROCESS Process = GetCurrentProcess();

	status = MmuIsBufferValid(ThreadId, sizeof(TID), PAGE_RIGHTS_READWRITE, Process);
	if (!SUCCEEDED(status))
	{
		return status;
	}

	if (UM_INVALID_HANDLE_VALUE == ThreadHandle)
	{
		PTHREAD pThread = GetCurrentThread();
		*ThreadId = ThreadGetId(pThread);
	}
	else
	{
		PUM_HANDLE_LIST_ENTRY pUmHandleListEntry = NULL;
		LIST_ITERATOR ProcessThreadIterator;
		ListIteratorInit(&Process->ProcessThreadDataList, &ProcessThreadIterator);
		PLIST_ENTRY pEntry;
		while ((pEntry = ListIteratorNext(&ProcessThreadIterator)) != NULL)
		{
			PUM_HANDLE_LIST_ENTRY pCurrentData = (PUM_HANDLE_LIST_ENTRY)CONTAINING_RECORD(pEntry, UM_HANDLE_LIST_ENTRY, ListEntry);
			if (pCurrentData->Handle == ThreadHandle)
			{
				pUmHandleListEntry = pCurrentData;
				break;
			}
		}

		if (NULL != pUmHandleListEntry)
		{
			ASSERT(pUmHandleListEntry->Type == HandleTypeThread);
			PTHREAD pThread = (PTHREAD)pUmHandleListEntry->Ptr;
			*ThreadId = ThreadGetId(pThread);
		}
		else
		{
			status = STATUS_INVALID_PARAMETER1;
		}
	}
	return status;
}

STATUS
SyscallThreadWaitForTermination(
	IN      UM_HANDLE               ThreadHandle,
	OUT     STATUS*                 TerminationStatus
)
{
	STATUS status = STATUS_SUCCESS;
	PPROCESS Process = GetCurrentProcess();

	if (UM_INVALID_HANDLE_VALUE == ThreadHandle)
	{
		return STATUS_INVALID_PARAMETER1;
	}

	status = MmuIsBufferValid(TerminationStatus, sizeof(STATUS), PAGE_RIGHTS_READWRITE, Process);
	if (!SUCCEEDED(status))
	{
		return status;
	}

	PUM_HANDLE_LIST_ENTRY pUmHandleListEntry = NULL;
	LIST_ITERATOR ProcessThreadIterator;
	ListIteratorInit(&Process->ProcessThreadDataList, &ProcessThreadIterator);
	PLIST_ENTRY pEntry;
	while ((pEntry = ListIteratorNext(&ProcessThreadIterator)) != NULL)
	{
		PUM_HANDLE_LIST_ENTRY pCurrentData = (PUM_HANDLE_LIST_ENTRY)CONTAINING_RECORD(pEntry, UM_HANDLE_LIST_ENTRY, ListEntry);
		if (pCurrentData->Handle == ThreadHandle)
		{
			pUmHandleListEntry = pCurrentData;
			break;
		}
	}

	if (NULL != pUmHandleListEntry)
	{
		ASSERT(pUmHandleListEntry->Type == HandleTypeThread);
		PTHREAD pThread = (PTHREAD)pUmHandleListEntry->Ptr;
		ThreadWaitForTermination(pThread, TerminationStatus);
	}
	else
	{
		status = STATUS_INVALID_PARAMETER1;
	}
	return status;
}

STATUS
SyscallThreadCloseHandle(
	IN      UM_HANDLE               ThreadHandle
)
{
	STATUS status = STATUS_SUCCESS;
	PPROCESS Process = GetCurrentProcess();

	if (UM_INVALID_HANDLE_VALUE == ThreadHandle)
	{
		return STATUS_INVALID_PARAMETER1;
	}

	PUM_HANDLE_LIST_ENTRY pUmHandleListEntry = NULL;
	LIST_ITERATOR ProcessThreadIterator;
	ListIteratorInit(&Process->ProcessThreadDataList, &ProcessThreadIterator);
	PLIST_ENTRY pEntry;
	while ((pEntry = ListIteratorNext(&ProcessThreadIterator)) != NULL)
	{
		PUM_HANDLE_LIST_ENTRY pCurrentData = (PUM_HANDLE_LIST_ENTRY)CONTAINING_RECORD(pEntry, UM_HANDLE_LIST_ENTRY, ListEntry);
		if (pCurrentData->Handle == ThreadHandle)
		{
			pUmHandleListEntry = pCurrentData;
			break;
		}
	}

	if (NULL != pUmHandleListEntry)
	{
		ASSERT(pUmHandleListEntry->Type == HandleTypeThread);
		INTR_STATE oldState;
		LockAcquire(&Process->ProcessThreadDataLock, &oldState);

		RemoveEntryList(pEntry);

		LockRelease(&Process->ProcessThreadDataLock, oldState);

		PTHREAD pThread = pUmHandleListEntry->Ptr;
		ThreadCloseHandle(pThread);

		VmmFreeRegionEx(pUmHandleListEntry, sizeof(UM_HANDLE_LIST_ENTRY), VMM_FREE_TYPE_DECOMMIT, FALSE, Process->VaSpace, Process->PagingData);
	}
	else
	{
		status = STATUS_INVALID_PARAMETER1;
	}

	return status;
}
/*
STATUS
SyscallProcessExit(
	IN      STATUS                  ExitStatus
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallProcessCreate(
	IN_READS_Z(PathLength)
	char*               ProcessPath,
	IN          QWORD               PathLength,
	IN_READS_OPT_Z(ArgLength)
	char*               Arguments,
	IN          QWORD               ArgLength,
	OUT         UM_HANDLE*          ProcessHandle
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallProcessGetPid(
	IN_OPT  UM_HANDLE               ProcessHandle,
	OUT     PID*                    ProcessId
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallProcessWaitForTermination(
	IN      UM_HANDLE               ProcessHandle,
	OUT     STATUS*                 TerminationStatus
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallProcessCloseHandle(
	IN      UM_HANDLE               ProcessHandle
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallVirtualAlloc(
	IN_OPT      PVOID                   BaseAddress,
	IN          QWORD                   Size,
	IN          VMM_ALLOC_TYPE          AllocType,
	IN          PAGE_RIGHTS             PageRights,
	IN_OPT      UM_HANDLE               FileHandle,
	IN_OPT      QWORD                   Key,
	OUT         PVOID*                  AllocatedAddress
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallVirtualFree(
	IN          PVOID                   Address,
	_When_(VMM_FREE_TYPE_RELEASE == FreeType, _Reserved_)
	_When_(VMM_FREE_TYPE_RELEASE != FreeType, IN)
	QWORD                   Size,
	IN          VMM_FREE_TYPE           FreeType
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallFileCreate(
	IN_READS_Z(PathLength)
	char*                   Path,
	IN          QWORD                   PathLength,
	IN          BOOLEAN                 Directory,
	IN          BOOLEAN                 Create,
	OUT         UM_HANDLE*              FileHandle
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallFileClose(
	IN          UM_HANDLE               FileHandle
)
{
	return STATUS_SUCCESS;
}

STATUS
SyscallFileRead(
	IN  UM_HANDLE                   FileHandle,
	OUT_WRITES_BYTES(BytesToRead)
	PVOID                       Buffer,
	IN  QWORD                       BytesToRead,
	OUT QWORD*                      BytesRead
)
{
	return STATUS_SUCCESS;
}
*/
STATUS
SyscallFileWrite(
	IN  UM_HANDLE                   FileHandle,
	IN_READS_BYTES(BytesToWrite)
	PVOID                       Buffer,
	IN  QWORD                       BytesToWrite,
	OUT QWORD*                      BytesWritten
)
{
	//LOGPL("FILE WRITE: %s\n", pSyscallParameters[1]);
	STATUS status = STATUS_SUCCESS;
	PPROCESS CurrentProcess = GetCurrentProcess();

	status = MmuIsBufferValid(Buffer, BytesToWrite, PAGE_RIGHTS_READ, CurrentProcess);
	if (!SUCCEEDED(status))
	{
		return status;
	}

	status = MmuIsBufferValid(BytesWritten, sizeof(QWORD), PAGE_RIGHTS_READWRITE, CurrentProcess);
	if (!SUCCEEDED(status))
	{
		return status;
	}

	if (FileHandle == UM_FILE_HANDLE_STDOUT)
	{
		LOGPL("[%s]:[%s]\n", ProcessGetName(NULL), Buffer); //Scrierea efectiva pe stdout
		*BytesWritten = strlen(Buffer) + 1;
		return STATUS_SUCCESS;
	}

	return STATUS_UNSUCCESSFUL;
}
